extern crate futures;
extern crate grpc;

use futures::Future;

use ByteVector;

use proto_client::rpc::{DeleteRangeRequest, PutRequest, RangeRequest};
use proto_client::rpc_grpc::{KVClient, KV};
use result_futures::{EtcdDeleteResult, EtcdPutResult, EtcdRangeResult, EtcdSingleRangeResult};

pub trait Client {
    fn new(address: &str, port: u16) -> EtcdClient;

    fn put_async(&self, key: ByteVector, value: ByteVector) -> EtcdPutResult;
    fn get_async(&self, key: ByteVector) -> EtcdSingleRangeResult;
    fn get_prefix_async(&self, key: ByteVector) -> EtcdRangeResult;
    fn get_range_async(&self, range_start: ByteVector, range_end: ByteVector) -> EtcdRangeResult;
    fn delete_async(&self, key: ByteVector) -> EtcdDeleteResult;

    fn put(&self, key: ByteVector, value: ByteVector) -> Result<grpc::Metadata, grpc::Error>;
    fn get(&self, key: ByteVector) -> Result<(Option<ByteVector>, grpc::Metadata), grpc::Error>;
    fn get_prefix(
        &self,
        key: ByteVector,
    ) -> Result<(Option<Vec<(ByteVector, ByteVector)>>, grpc::Metadata), grpc::Error>;
    fn get_range(
        &self,
        range_start: ByteVector,
        range_end: ByteVector,
    ) -> Result<(Option<Vec<(ByteVector, ByteVector)>>, grpc::Metadata), grpc::Error>;
    fn delete(&self, key: ByteVector) -> Result<grpc::Metadata, grpc::Error>;
}

pub struct EtcdClient {
    grpc_client: grpc::Client,
}

#[derive(Default)]
pub struct EtcdRangeRequest {
    range_request: RangeRequest,
}

impl EtcdRangeRequest {
    pub fn key(mut self, key: ByteVector) -> Self {
        self.range_request.set_key(key);
        self
    }
    pub fn range_end(mut self, range_end: ByteVector) -> Self {
        self.range_request.set_range_end(range_end);
        self
    }
    pub fn limit(mut self, limit: i64) -> Self {
        self.range_request.set_limit(limit);
        self
    }
}

impl Client for EtcdClient {
    fn new(address: &str, port: u16) -> EtcdClient {
        EtcdClient {
            grpc_client: grpc::Client::new_plain(address, port, Default::default()).unwrap(),
        }
    }

    fn put_async(&self, key: ByteVector, value: ByteVector) -> EtcdPutResult {
        let put_request = build_put_request(key, value);
        EtcdPutResult {
            inner: KVClient::with_client(self.grpc_client.clone())
                .put(grpc::RequestOptions::new(), put_request)
                .join_metadata_result(),
        }
    }

    fn get_async(&self, key: ByteVector) -> EtcdSingleRangeResult {
        let get_request: EtcdRangeRequest = Default::default();
        EtcdSingleRangeResult {
            inner: KVClient::with_client(self.grpc_client.clone())
                .range(
                    grpc::RequestOptions::new(),
                    get_request.key(key).range_request,
                ).join_metadata_result(),
        }
    }

    fn get_range_async(&self, range_start: ByteVector, range_end: ByteVector) -> EtcdRangeResult {
        let get_request: EtcdRangeRequest = Default::default();
        EtcdRangeResult {
            inner: KVClient::with_client(self.grpc_client.clone())
                .range(
                    grpc::RequestOptions::new(),
                    get_request
                        .key(range_start)
                        .range_end(range_end)
                        .range_request,
                ).join_metadata_result(),
        }
    }

    fn get_prefix_async(&self, prefix: ByteVector) -> EtcdRangeResult {
        self.get_range_async(prefix.clone(), fake_increment_last_byte(prefix.clone()))
    }

    fn delete_async(&self, key: ByteVector) -> EtcdDeleteResult {
        let mut delete_request: DeleteRangeRequest = Default::default();
        delete_request.set_key(key);
        EtcdDeleteResult {
            inner: KVClient::with_client(self.grpc_client.clone())
                .delete_range(grpc::RequestOptions::new(), delete_request)
                .join_metadata_result(),
        }
    }

    fn put(&self, key: ByteVector, value: ByteVector) -> Result<grpc::Metadata, grpc::Error> {
        self.put_async(key, value).wait()
    }

    fn get(&self, key: ByteVector) -> Result<(Option<ByteVector>, grpc::Metadata), grpc::Error> {
        self.get_async(key).wait()
    }

    fn get_range(
        &self,
        range_start: ByteVector,
        range_end: ByteVector,
    ) -> Result<(Option<Vec<(ByteVector, ByteVector)>>, grpc::Metadata), grpc::Error> {
        self.get_range_async(range_start, range_end).wait()
    }

    fn get_prefix(
        &self,
        key: ByteVector,
    ) -> Result<(Option<Vec<(ByteVector, ByteVector)>>, grpc::Metadata), grpc::Error> {
        self.get_prefix_async(key).wait()
    }

    fn delete(&self, key: ByteVector) -> Result<grpc::Metadata, grpc::Error> {
        self.delete_async(key).wait()
    }
}

fn build_put_request(key: ByteVector, value: ByteVector) -> PutRequest {
    let mut put_request = PutRequest::new();
    put_request.set_key(key);
    put_request.set_value(value);
    put_request
}

fn fake_increment_last_byte(key: ByteVector) -> ByteVector {
    let mut return_val = key;
    let len = return_val.len();
    return_val[len - 1] += 1;
    return_val
}
