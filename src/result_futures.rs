extern crate futures;
extern crate grpc;
extern crate protobuf;

use std::vec::Vec;

use futures::{Async, Future, Poll};

use proto_client::rpc::{DeleteRangeResponse, PutResponse, RangeResponse};

use ByteVector;

pub struct EtcdSingleRangeResult {
    pub inner: grpc::GrpcFuture<(grpc::Metadata, RangeResponse, grpc::Metadata)>,
}

impl Future for EtcdSingleRangeResult {
    type Item = (Option<ByteVector>, grpc::Metadata);
    type Error = grpc::Error;

    fn poll(&mut self) -> Poll<Self::Item, Self::Error> {
        match self.inner.poll() {
            Ok(Async::Ready((metadata, _range_response, _))) => match _range_response.kvs.first() {
                None => Ok(Async::Ready((None, metadata))),
                Some(elem) => Ok(Async::Ready((Some(elem.get_value().to_vec()), metadata))),
            },
            Ok(Async::NotReady) => Ok(Async::NotReady),
            Err(err) => Err(err),
        }
    }
}

pub struct EtcdRangeResult {
    pub inner: grpc::GrpcFuture<(grpc::Metadata, RangeResponse, grpc::Metadata)>,
}

impl Future for EtcdRangeResult {
    type Item = (Option<Vec<(ByteVector, ByteVector)>>, grpc::Metadata);
    type Error = grpc::Error;

    fn poll(&mut self) -> Poll<Self::Item, Self::Error> {
        match self.inner.poll() {
            Ok(Async::Ready((metadata, range_response, _))) => {
                if range_response.kvs.len() == 0 {
                    Ok(Async::Ready((None, metadata)))
                } else {
                    Ok(Async::Ready((
                        Some(
                            range_response
                                .kvs
                                .into_iter()
                                .map(|x| (x.key, x.value))
                                .collect(),
                        ),
                        metadata,
                    )))
                }
            }
            Ok(Async::NotReady) => Ok(Async::NotReady),
            Err(err) => Err(err),
        }
    }
}

pub struct EtcdPutResult {
    pub inner: grpc::GrpcFuture<(grpc::Metadata, PutResponse, grpc::Metadata)>,
}

impl Future for EtcdPutResult {
    type Item = grpc::Metadata;
    type Error = grpc::Error;

    fn poll(&mut self) -> Poll<Self::Item, Self::Error> {
        match self.inner.poll() {
            Ok(Async::Ready((metadata, _put_response, _))) => Ok(Async::Ready(metadata)),
            Ok(Async::NotReady) => Ok(Async::NotReady),
            Err(err) => Err(err),
        }
    }
}

pub struct EtcdDeleteResult {
    pub inner: grpc::GrpcFuture<(grpc::Metadata, DeleteRangeResponse, grpc::Metadata)>,
}

impl Future for EtcdDeleteResult {
    type Item = grpc::Metadata;
    type Error = grpc::Error;
    fn poll(&mut self) -> Poll<Self::Item, Self::Error> {
        match self.inner.poll() {
            Ok(Async::Ready((metadata, _delete_response, _))) => Ok(Async::Ready(metadata)),
            Ok(Async::NotReady) => Ok(Async::NotReady),
            Err(err) => Err(err),
        }
    }
}
