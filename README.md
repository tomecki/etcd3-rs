# etcd-rs

[![pipeline status](https://gitlab.com/tomecki/etcd3-rs/badges/master/pipeline.svg)](https://gitlab.com/tomecki/etcd3-rs/commits/master)
[![coverage report](https://gitlab.com/tomecki/etcd3-rs/badges/master/coverage.svg)](https://gitlab.com/tomecki/etcd3-rs/commits/master)


WIP implementation of rust's bindings to etcd3.
