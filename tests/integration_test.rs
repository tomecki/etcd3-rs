extern crate config;
extern crate etcd3;
extern crate futures;

use etcd3::client::{Client, EtcdClient};
use futures::Future;

fn vectorize(text: &str) -> Vec<u8> {
    text.as_bytes().to_vec()
}

fn provide_configuration() -> config::Config {
    let mut settings = config::Config::default();
    settings
        .merge(config::File::with_name("config/dev.toml"))
        .unwrap()
        .merge(config::Environment::with_prefix("etcd_test").separator("_"))
        .unwrap();
    settings
}

#[test]
fn test_key_put_get_delete() {
    let configuration = provide_configuration();
    let client = EtcdClient::new(
        &configuration.get::<String>("etcd3.host").unwrap(),
        configuration.get::<u16>("etcd3.port").unwrap(),
    );

    client.put(vectorize("asdf"), vectorize("wqer")).unwrap();
    let (result, _) = client.get_async(vectorize("asdf")).wait().unwrap();
    assert!(result == Some(vectorize("wqer")));

    client.delete(vectorize("asdf")).unwrap();
    let (empty_result, _) = client.get_async(vectorize("asdf")).wait().unwrap();
    assert!(empty_result == None)
}

#[test]
fn test_get_prefix() {
    let configuration = provide_configuration();
    let client = EtcdClient::new(
        &configuration.get::<String>("etcd3.host").unwrap(),
        configuration.get::<u16>("etcd3.port").unwrap(),
    );
    client
        .put(vectorize("asdf_qwer"), vectorize("wqer"))
        .unwrap();
    client
        .put(vectorize("asdf_qwet"), vectorize("wqer"))
        .unwrap();
    client
        .put(vectorize("asdf_qwes"), vectorize("wqer"))
        .unwrap();
    let (result, _) = client.get_prefix_async(vectorize("asdf_")).wait().unwrap();
    assert!(result.unwrap().len() == 3)
}
